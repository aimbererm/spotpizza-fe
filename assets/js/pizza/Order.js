app.service('Order',function($rootScope,$http,$location){
    var me = {};

    me.cart = [];
    me.off = 0;
    me.total = 0;
    me.borda = {};
    me.tamanho = {};
    me.submitButton = true;

    me.add = function(pizza) {
        me.cart.push(pizza);
    }

    me.remove = function(i){
        me.cart.splice(i,1);
    }

    me.clear = function(){
        me.cart = [];
        me.off = 0;
        me.total = 0;
    }

    me.setBorda = function(pizza,border){
        pizza.borda = border;
        
    }

    me.setTamanho = function(pizza,size){
        pizza.tamanho = size;
        
    }

    me.verifySizesBorders = function(){
        
        for(var i = 0;i< me.cart.length;i++){
            if(me.cart[i].borda == undefined || me.cart[i].tamanho == undefined){
                me.submitButton = true;
                return;
                
            }else
                me.submitButton = false;
        }
        
    }

    me.go = function ( path ) {
        $location.path( path );
    };

    me.closeModal = function(){
        $("#cartModal").modal("hide");
    }

    return me;

})