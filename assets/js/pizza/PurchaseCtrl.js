'use strict';

app.controller('PurchaseCtrl',function($scope,Order){
    $scope.order = Order.cart;
    
    $scope.time = Math.floor(Math.random()*41)+20;

    $scope.total = function(){
        var totalCart = 0;

        for(var i = 0;i< $scope.order.length;i++){
            totalCart += ($scope.order[i].price+$scope.order[i].borda.valor)*$scope.order[i].tamanho.peso;
            
        }

        return totalCart;
    }

})