'use strict';

app.directive('cartList', function () {
	return {
		restrict: "E",
		templateUrl: "assets/js/pizza/carrinho.html",
		scope: {
			"list": "=",
            "order": "=",
            "size": "=",
            "border": "="
		}
	}
});