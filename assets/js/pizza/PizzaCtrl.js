'use strict';

app.controller("PizzaCtrl",function($scope,SaborRsrc,TamanhoRsrc,BordaRsrc,Order){

    $scope.list = [];
    $scope.price = function(o){
        var sum = 0;
        
        for(var i in o.ingredientes){
            sum+= o.ingredientes[i].preco;
        }
        return o.price = sum;    
        };

    SaborRsrc.query(function(data){
        $scope.list = data;
    });

    TamanhoRsrc.query(function(data){
        $scope.size = data;
    });

    BordaRsrc.query(function(data){
        $scope.border = data;
    });


    $scope.order = Order;
    
    
})