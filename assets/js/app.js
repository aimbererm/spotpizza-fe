'use strict';

var app = angular.module('pizza', ['ngRoute', 'ngResource']);

app.config(function ($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl: 'assets/js/pizza/sabores.html',
			controller: 'PizzaCtrl'
		})
		.when('/purchase', {
			templateUrl: 'assets/js/pizza/purchase.html',
			controller: 'PurchaseCtrl'
		})
		
		.otherwise({
			redirectTo: '/'
		});
});